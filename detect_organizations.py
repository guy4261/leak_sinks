# -*- coding: utf-8 -*-

import gzip
import os
import sys
import traceback

import igraph as ig

MIN_SIZE_TO_DUMP = 10000 #300

class CommunityMultilevelDriver():
    """A driver for igraph's multilevel community detection algorithm.
    http://igraph.org/python/doc/igraph.Graph-class.html#community_multilevel"""
    
    def __init__(self):
        self.real2virt = None
        self.virt2real = None
        self.edges = None
        self.g = None
        self.communities = None
        self.filename = None
    
    def read_gzipped_edgelist(self, filename,
            skip_header=True, delimiter=",", src_column=0, dst_column=1):
        """Reads an edgelist, which may or may not be gzipped.


        filename - the input filename
        skip_header - whether or not to skip the first line (default=True)
        delimiter - string to consider as the delimiter (default=",")
        src_column - the 


        """
        
        self.filename = filename
        spaces = "\r\n\t "
        
        fopen = gzip.open if filename.endswith(".gz") else open
        
        real2virt = {}
        edges = set()

        with fopen(filename) as f:
            if skip_header:
                header = f.next().strip(spaces)
                
                for potential_delimiter in ["\t", ",", " "]:
                    if potential_delimiter in header:
                        delimiter = potential_delimiter
                        break
                
                header = header.split(delimiter)
                header = [_.strip(""" "' """) for _ in header]
            
                if isinstance(src_column, (str, unicode)):
                    try:
                        src_column = src_column.strip(""" "' """)
                        src_column = header.index(src_column)
                    except:
                        raise ValueError("Can't find source column %s in header %s" %(src_column, str(header)))
                        exit(-1)
                        
                if isinstance(dst_column, (str, unicode)):
                    try:
                        dst_column = dst_column.strip(""" "' """)
                        dst_column = header.index(dst_column)
                    except:
                        raise ValueError("Can't find destination column %s in header %s" %(dst_column, str(header)))
                        exit(-1)
                
                try:
                    
                    int(header[src_column])
                    int(header[dst_column])                    
                    f.seek(0)
                except:
                    traceback.print_exc(file=sys.stdout)                
                    print "Tried not to skip header but couldn't"                        
                
                
            for row in f:
                row = row.strip(spaces)
                if len(row) > 0:
                    cells = row.split(delimiter)
                    cells = [_.strip(""""'""") for _ in cells]
                    id1 = int(cells[src_column])
                    id2 = int(cells[dst_column])
                    if id1 not in real2virt:
                        real2virt[id1] = len(real2virt)
                    if id2 not in real2virt:
                        real2virt[id2] = len(real2virt)
                    
                    id1v = real2virt[id1]
                    id2v = real2virt[id2]
                    #edges are virtual
                    edges.add((id1v, id2v))

        g = ig.Graph()
        g.add_vertices(len(real2virt))
        g.add_edges(edges)
        print "Loaded:", g.summary()
        
        self.real2virt = real2virt
        self.edges = edges
        self.g = g


    def run_community_multilevel(self, algorithm):
        """Run igraph's multilevel community detection."""
        g = self.g
        
        
        func = getattr(g, algorithm)
        communities = func()
        print "Modularity:", g.modularity(communities)
        
        self.communities = communities
        

    def dump_communities(self, prefix=None):
        """Dump the detected communities to a set files.
        
        The files' naming convention is "<edgelist filename>_..." unless
        a different prefix is given.
        
        A summary file, listing each community's ID, |V| and |E| is created: <prefix>summary.                
        Each community's edgelist is created: <prefix>community_<id>.        
        """
        
        real2virt = self.real2virt
        communities = self.communities
        g = self.g
        edges = self.edges
        
        if prefix is None:
            prefix = self.filename + "_"
        
        virt2real = (self.virt2real if self.virt2real is not None
            else {v: k for k, v in real2virt.iteritems()})
               
        self.virt2real = virt2real
        z = len(str(len(communities)))
        
        #now communities are virtual ids        
        communities = [set([v for v in c]) for c in communities]
               
        out = open(prefix + "summary",'wb')
        out.write("community id\tV\tE\n")

        for i, comm in enumerate(communities):
            if len(comm) < MIN_SIZE_TO_DUMP:
                continue
            community_edges = set()
            for (v1, v2) in edges:
                if v1 in comm and v2 in comm:
                    r1, r2 = virt2real[v1], virt2real[v2]
                    community_edges.add((r1, r2))
            V = len(comm)
            E = len(community_edges)
            out.write("%d\t%d\t%d\n" % (i, V, E))
            
            fname = prefix + "community_" + str(i).zfill(z) + ".csv"
            with open(fname, 'wb') as o:
                for e in community_edges:
                    o.write("%d,%d\n" % e)


def assert_writable(prefix):
    if prefix.endswith(os.path.sep) and not os.path.isdir(prefix):
        try:
            os.makedirs(prefix)
        except:
            return False
    return os.access(prefix, os.W_OK)
        

if __name__ == "__main__":
    import sys
    try:
        filename = sys.argv[1]
        
        prefix = sys.argv[2]
        
        src_column = sys.argv[3]
        try:
            src_column = int(src_column)
        except:
            pass
            
        dst_column = sys.argv[4]
        try:
            dst_column = int(dst_column)
        except:
            pass
        
    except:
        print """Usage: %s <filename> <output prefix> <source column> <dst column>
        filename - edgelist of target graph.
        output prefix - prefix to output files; "<filename>_" is used if not supplied.
        src column - 0-based index of source column (or name if there is a header)
        dst column - 0-based index of destination column (or name if there is a header)        
        Exiting...""" % (sys.argv[0])
        exit(-1)
       
    for algorithm in [
        #'community_fastgreedy',
        'community_multilevel',
        #'community_edge_betweenness', 
        #'community_infomap', 'community_label_propagation', #already tried them!
        #'community_leading_eigenvector_naive',
        #'community_walktrap',
        #'community_spinglass', 
        #'community_leading_eigenvector',
        #'community_optimal_modularity'
]:
        
        try:            
            #_prefix = prefix + "_" + algorithm + os.path.sep
            _prefix = prefix + (os.path.sep if not prefix.endswith(os.path.sep) else "")

            if not assert_writable(_prefix):
                print _prefix, "is not writable! Aborting!"
                exit(-1)


            driver = CommunityMultilevelDriver()
            driver.read_gzipped_edgelist(filename, src_column=src_column, dst_column=dst_column)
            driver.run_community_multilevel(algorithm)
            driver.dump_communities(_prefix)
        except:

            traceback.print_exc(file=sys.stdout)
            print "Community detection with %s failed! Exiting..." % (algorithm)
            exit(-1)

