# Leak Sinks: The Threat of Targeted Social Eavesdropping
### Yasmin Bokobza, Abigail Paradise, Guy Rapaport, Rami Puzis, Bracha Shapira and Asaf Shabtai


## Overview

This is the code we used in our paper. You may download it and try it out on
the flickr dataset of Cha et al. or use it on your own data. We will be glad
to support, fix bugs and correspond. You can e-mail Guy Rapaport at
[guy4261 *at* gmail.com](mailto:guy4261@gmail.com).


## Long Story Short

Run ```setup.py``` to install the necessary Python packages to run this code,
and ```run.py``` to download the flickr dataset and run the experiment on it.

```bash
$ chmod u+x setup.py
$ chmod u+x run.py
$ ./setup.py
$ ./run.py
```

## Setup

To use this code, you will have to install some packages we used. The first is
the igraph Python API (python-igraph). This can be installed using pip:

```bash
$ pip install python-igraph
$ pip install networkx
```

Full installation instructions can be
found [here](https://pypi.python.org/pypi/python-igraph). If installation via
pip fails, these instructions should be consulted.


## Obtaining the flickr dataset and detecting organizations

In the paper, we analyze the flickr dataset of Cha, Misolve and Gummadi.
Their data is publicly available at:

http://socialnetworks.mpi-sws.org/data-www2009.html

In particular, to demonstrate our work we require the list of links, which is
available from here:

http://socialnetworks.mpi-sws.mpg.de/data/flickr-growth.txt.gz

The following script will download the network and apply the multilevel
community detection algorithm to it. We consider the detected communities to be
the organizations targeted by the attacker.

```bash
# Download the dataset
$ DATADIR=data/flickr
$ mkdir -p ${DATADIR}
$ wget -NP ${DATADIR} \
	http://socialnetworks.mpi-sws.mpg.de/data/flickr-growth.txt.gz
 
# Run the community detection
$ python detect_organizations.py ${DATADIR}/flickr-growth.txt flickr 1 0
```

The resulting communities/organiations will be placed under ```data/flickr```.


## Looking for Sensitive Information Leakages in the Detected Organizations

```bash
$ DATADIR=data/flickr
$ wget -NP ${DATADIR} \
	http://socialnetworks.mpi-sws.mpg.de/data/flickr-all-photos.txt.gz
$ wget -NP ${DATADIR} \
http://socialnetworks.mpi-sws.mpg.de/data/flickr-all-photo-favorite-markings.txt.gz
$ python buildDataFlickr.py ${DATADIR}/flickr-all-photos.txt.gz
$ python calculateCostFlickr.py ${DATADIR}/flickr-all-photos.txt.gz
```


## Attack, Phase I: Simulating the Wiring Process

```bash
# simulate wiring
$ python SimulateWiring.py "data/flickr/community_*.csv" 300 1000000 all
```


## Attack, Phase II: Simulating the Monitoring Process

```bash
# simulate monitoring
$ python SimulateMonitoring.py
```


## Colophon

We used ```gimli``` to convert this markdown to PDF.
You can install it according to the instructions here:
http://kevin.deldycke.com/2012/01/how-to-generate-pdf-markdown/
