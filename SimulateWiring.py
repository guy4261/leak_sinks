import sys
import traceback
import os
from glob import glob

from networkx import read_edgelist, DiGraph

from socialbots.Random_fakeProfile import random_fakeProfile
from socialbots.PageRank_fakeProfile import PageRank_fakeProfile
from socialbots.ReversePageRank_fakeProfile import ReversePagerank_fakeProfile
from socialbots.MickyFire_fakeProfile import MickyFire_fakeProfile
from socialbots.Abigail_fakeProfile import Abigail_fakeProfile
from socialbots.indegree_fakeprofile import indegree_fakeprofile

from socialbots.PrintToFile import PrintToFile

from datetime import datetime


def now():
    return str(datetime.now())

NUM_ITERATIONS = 3

models = {
    "random": random_fakeProfile,
    "PageRank": PageRank_fakeProfile,
    "ReversePageRank": ReversePagerank_fakeProfile,
    "MickyFire": MickyFire_fakeProfile,
    "Abigail": Abigail_fakeProfile,
    "InDegree": indegree_fakeprofile
    }

def to_yasmin_results(allAbigailResults):
    allYasminResults = {}
    accepted_so_far = 0
    for k, (k, nodeID, accepted) in allAbigailResults.iteritems():
        requests_so_far = k
        who_is_it = nodeID
        if accepted:
            accepted_so_far += 1
            fail_so_far = requests_so_far - accepted_so_far
            allYasminResults[accepted_so_far] = (accepted_so_far, who_is_it, requests_so_far, fail_so_far)
    return allYasminResults


if __name__ == "__main__":
    
    outfiles = []
    
    DEFAULT_MINIMUM = 500
    DEFAULT_MAXIMUM = 3000
    
    try:
        paths = sys.argv[1]
        min_nodes = int(sys.argv[2]) if len(sys.argv) > 2 else DEFAULT_MINIMUM
        max_nodes = int(sys.argv[3]) if len(sys.argv) > 3 else DEFAULT_MAXIMUM
        model = sys.argv[4] if len(sys.argv) > 4 else "all"
        if model.lower() != "all":
            models = {k: v for k, v in models.iteritems() if k == model}
    
    except:
        print "Usage: %s <edgelist file> [<min_nodes=2> [<max_nodes=10000>]]" % (sys.path[0])
        exit(-1)
    
    print "Using communities in [%d, %d]" % (min_nodes, max_nodes)
    
    paths = glob(paths)
    paths = [_ for _ in paths if "_wiring_" not in _]
    paths = [_ for _ in paths if "_friendsTable_" not in _]
    paths = [_ for _ in paths if not _.endswith("_data.csv")]
    paths = [_ for _ in paths if not _.endswith("_full.csv")]
    paths = [_ for _ in paths if not _.endswith("_exposures.csv")]
    print paths

    sorted_models = sorted(models.items())
    nr_models = len(sorted_models)
    nr_paths = len(paths)
    #sorted_models = [("random", random_fakeProfile)] #for debugging
    print "Order of models:", [m[0] for m in sorted_models]
    print "Number of files:", nr_paths
    
    for file_index, filename in enumerate(paths):
    
#        print "Current filename:", filename

        for (model_name, model_func) in sorted_models:
#            print "Current model:", model_name
            for iteration in xrange(NUM_ITERATIONS):
                #resumable
                outfile = filename.rsplit(".", 1)[0] + "_%s_friendsTable_%02d.csv" % (model_name, iteration)
                yasminfile = filename.rsplit(".", 1)[0] + "_%s_wiring_%02d.csv" % (model_name, iteration)
                if os.path.isfile(outfile) and os.path.isfile(yasminfile):
                    continue
                
                print now(), filename, model_name, "iteration %d/%d" % (iteration+1, NUM_ITERATIONS), "File %d/%d" % (file_index+1, nr_paths),

                graph = read_edgelist(filename, delimiter=",", comments="#",
                    nodetype=int, create_using=DiGraph())
                print "Graphsize: V=%d , E=%d" % (len(graph), graph.size())

                if not min_nodes <= len(graph) <= max_nodes:
                    print "Graph too big! Skipping."
                    break

                try:                
                    fp = model_func(graph, "in")
                    results = fp.run()
                    PrintToFile.csvFile(results, outfile)
                    
                    yasmin_results = to_yasmin_results(results)
                    PrintToFile.yasminFile(yasmin_results, yasminfile)
                    
                    outfiles.append(filename)
                    outfiles.append(outfile)
                    outfiles.append(yasminfile)                
                except:
                    print "Failed in",file_index, filename, model_name, iteration
                    traceback.print_exc(file=sys.stdout)
                    continue
    
    for fname in outfiles:
        print fname

