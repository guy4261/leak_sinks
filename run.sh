#!/bin/bash

DATADIR=data/flickr

wget -NP ${DATADIR} \
	http://socialnetworks.mpi-sws.mpg.de/data/flickr-growth.txt.gz

python detect_organizations.py ${DATADIR}/flickr-growth.txt.gz data/flickr 1 0

wget -NP ${DATADIR} \
	http://socialnetworks.mpi-sws.mpg.de/data/flickr-all-photos.txt.gz
wget -NP ${DATADIR} \
	http://socialnetworks.mpi-sws.mpg.de/data/flickr-all-photo-favorite-markings.txt.gz
python buildDataFlickr.py "data/flickr/community_*.csv"
python calculateCostFlickr.py "data/flickr/community_*.csv"

# simulate wiring
python SimulateWiring.py "data/flickr/community_*.csv" 300 1000000 all

# simulate monitoring
python SimulateMonitoring.py
