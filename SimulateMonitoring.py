import glob
import csv
import os
import sys
import collections 
import datetime
from os.path import isfile

from collections import defaultdict, Counter
import networkx as nx
import sys


def now():
    return str(datetime.datetime.now())

def is_empty(filename):
    statinfo = os.stat(filename)
    return statinfo.st_size == 0


def are_all_available(edges_file, datafile, exposures_file, wiring_file, verbose=False):
    available = map(isfile, [edges_file, datafile, exposures_file, wiring_file])    
    all_available = all(available)
    if all_available:
        return True
    else:
        if verbose:
            print "Missing files:"
            for fname, availability in zip([edges_file, datafile, exposures_file, wiring_file], available):
                if not availability:
                    print fname
            print
        return False


def extract_datagiven_from_data_file(datafile):
    """old format"""
    datagiven=collections.defaultdict(dict)
    f = file(datafile, "r")
    #f.next() #skip header
    
    #prop, annotation, args =~ token_id, timestamp_id, numeric
    #datagiven[token_id, numeric] = annotation        
    #factsReader = csv.DictReader(f, fieldnames=["prop", "annotation"], restkey="args", restval=0)
    factsReader = csv.DictReader(f, fieldnames=["token_id", "timestamp_id", "numeric"], restkey="args", restval=0)
    for record in factsReader:
        
        token_id = record["token_id"]
        if token_id == "token_id":
            continue
        else:
           token_id = int(token_id)
        timestamp_id = int(record["timestamp_id"])
        numeric = int(record["numeric"])
        datagiven[token_id][numeric] = timestamp_id
    f.close()
    return datagiven


def read_exposures_file(exposures_file):
    NumberOfExposures = Counter()
    with open(exposures_file) as f:
        f.next() # skip header userid, exposures
        for row in f:
            row = row.strip("\r\n\t ")
            if len(row) == 0:
                continue
            user,numberE = row.split(',') 
            user = int(user)
            numberE = int(numberE)
            NumberOfExposures[user] = numberE
                
    return NumberOfExposures


def find_leakages(datagiven,
    NumberOfExposures,
    wiring_file,
    outfile
    ):
    
    nodesToMonitor=set()
    CostMonitored={}
    NumOfLeakages={}
    friendAchieved={}
    
    opSource=set()
    num = 1
    
    with open(wiring_file, "rb") as c:
        header = c.next()#skip header     
        reader = csv.reader(c, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
        for row in reader:
            num, node, requestsSoFar, failSoFa= row
            num=int(num)
            node=int(node)
            friendAchieved[num]=node
            CostMonitored[num]=0
            NumOfLeakages[num]=0
            """
            if node in NumberOfExposures:
                amount = NumberOfExposures[node]
                if amount > 0:
                    print "Step %d, got user %d, exposures: %d" % (num, node, NumberOfExposures[node])
            """

    for num in friendAchieved:
        n=friendAchieved[num] 
        nodesToMonitor.add(friendAchieved[num])
        for node in nodesToMonitor:
            CostMonitored[num]+=NumberOfExposures[node]

        for key in datagiven:
            dataG=datagiven[key]
            if n in dataG:
                if(key not in opSource): 
                    opSource.add(key)

            if key in opSource:
                NumOfLeakages[num] += 1
    start = datetime.datetime.now() 

    #write to file
    print "write to files"
    with open(outfile, 'wb') as out:
        writer=csv.writer(out, delimiter=',')
        data = [["k","who is it","NumOfLeakages","CostMonitoring",],]
        writer.writerows(data)
        for num in friendAchieved:
            data = [[num, friendAchieved[num], NumOfLeakages[num], CostMonitored[num], ], ]
            writer.writerows(data)


def prepare_lines(summary_file):
    with open(summary_file) as f:
        lines = f.readlines()[1:]#skip header
    lines = [_.strip("\r\n\t ") for _ in lines]
    lines = [_ for _ in lines if len(_) > 0]
    lines = map(lambda line:map(int, line.split("\t")), lines)        
    lines = sorted(lines, key=lambda t:t[1]) # by V, ascending
    return lines


def flickr_iterator():
    base = "data/flickr/"
    summary_file = base + "summary"
    DEFAULT_MINIMUM = 300
    DEFAULT_MAXIMUM = 60000

    if len(sys.argv) > 1:
        alg = sys.argv[1]
        ws = ["community_%05d_" + alg + "_wiring_%02d.csv",]
    else:
        ws = [
            "community_%05d_random_wiring_%02d.csv",
            "community_%05d_Abigail_wiring_%02d.csv",
            "community_%05d_MickyFire_wiring_%02d.csv",
            "community_%05d_PageRank_wiring_%02d.csv",
            "community_%05d_ReversePageRank_wiring_%02d.csv",
            "community_%05d_InDegree_wiring_%02d.csv",
            ]

    lines = prepare_lines(summary_file)
    nr_files = len(lines)

    for file_index, (cid, V, E) in enumerate(lines):

        if DEFAULT_MINIMUM <= V <= DEFAULT_MAXIMUM:

            datafile = base + "community_%05d.csv_data.csv" % (cid)                
            exposures_file = base + "community_%05d.csv_exposures.csv" % (cid)
            if not (isfile(datafile) and isfile(exposures_file)):
                #print "bye1"
                continue

            if is_empty(datafile):
                continue
            
            datagiven = None
            NumberOfExposures = None
            
            for w in ws:
                for i in xrange(50):
                    wiring_file = base + w % (cid, i)
                    if not isfile(wiring_file):
                        #print "bye2",
                        continue
                    
                    outfile = wiring_file + ".leakages.csv"                        
                    if isfile(outfile):
                        print "Skip!", outfile
                        continue #already done that.
                                            
                    print now(), "Community %d size=%d" %(cid, V), "File %d/%d," % (file_index + 1, nr_files), wiring_file
                    
                    if datagiven is None:
                        datagiven = extract_datagiven_from_data_file(datafile)
                    if NumberOfExposures is None:
                        NumberOfExposures = read_exposures_file(exposures_file)


                    find_leakages(datagiven, NumberOfExposures, wiring_file, outfile)


if __name__ == "__main__":    
    flickr_iterator()
