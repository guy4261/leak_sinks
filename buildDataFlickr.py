import csv
import collections
import datetime
import os
import re
import sys
import traceback

from glob import glob
from gzip import open as _gz_open


def now():
    return str(datetime.datetime.now())


def cache_flickr_all_photos():
    print now(), "starting cache flickr all photos"
    flickr_all_photos = "data/flickr/flickr-all-photos.txt.gz"
    #flickr_all_photos = "flickr-all-photos.txt"
    print "choose photos of group "
    ret = []
    with _gz_open(flickr_all_photos, "r") as infile:
        for line in infile:
            cells = line.strip().split("\t")
            if len(cells) != 6:
                continue
            photoID, timeUp, owner, temp1, temp2,temp3=cells
            
            photoID=int(photoID)
            timeUp=int(timeUp)
            owner=int(owner)
            
            ret.append( (photoID, timeUp, owner) )
    print now(), "done caching flickr all photos"
    return ret


def cache_flickr_all_photo_favorite_markings():
    print now(), "starting cache flickr all fav marks"
    flickr_all_photo_favorite_markings = "data/flickr/flickr-all-photo-favorite-markings.txt.gz"
    #flickr_all_photo_favorite_markings = "flickr-all-photo-favorite-markings.txt"    
    ret = []
    
    with _gz_open(flickr_all_photo_favorite_markings, "r") as infile:
        for line in infile:
            userId,photoID,timeLike=line.strip().split('\t')  
            userId=int(userId)
            photoID=int(photoID)
            timeLike=int(timeLike)
            ret.append( (userId, photoID, timeLike) )
    print now(), "done cache flickr all fav marks"
    return ret

                
flickr_all_photos = cache_flickr_all_photos()
flickr_all_photo_favorite_markings = cache_flickr_all_photo_favorite_markings()


def calc_data(community_file, outfile):

    nodes = set()

    ################################################################

    for line in open(community_file, "r"):
        lines = line.splitlines()
        for line in lines:
            node1,node2=line.strip().split(",")
            node1=int(node1)
            node2=int(node2)
            nodes.add(node1)
            nodes.add(node2)            
    
    size = len(nodes)
    print "size of network:", size
    if size < 300 or size > 60000:
        return
    
                    
    ########################################################
    finalPhotos=set()
    photos=collections.defaultdict(list)
    print "choose photos of group "
    start = datetime.datetime.now()
    
    for (photoID, timeUp, owner) in flickr_all_photos:
        if owner in nodes: 
            photos[photoID].append((timeUp,owner))         
            finalPhotos.add(photoID)
    
    stop = datetime.datetime.now()
    print "choose photos of group takes"+str(stop - start)
    #####################################################################
    
    print "choose users that likes this photos"
    start = datetime.datetime.now() 
    try:
        outfile1 = file(outfile, "wb")
        writer1=csv.writer(outfile1,delimiter=',')
        
        for (userId,photoID,timeLike) in flickr_all_photo_favorite_markings:
            if photoID in finalPhotos:
                photos[photoID].append((timeLike,userId))

        j=0
        for key in photos.keys():
            i=0
            for (timeLike, userId) in photos[key]:
                data = [[str(j) ,i, userId],] 
                writer1.writerows(data)
                i=i+1
            j=j+1 
        outfile1.close()        

    except IOError as err:
        print "I/O error: {0}".format(err)   
    stop = datetime.datetime.now()
    print "choose users that likes this photos takes"+str(stop - start)
                   
    ###################################################################


if __name__ == "__main__":
    
    try:
        path = sys.argv[1]
        #path = "data/flickr/flickr_community_multilevel/community_*.csv"
        files = glob(path)
    except:
        traceback.print_exc(file=sys.stdout)
        print "Usage: %s <glob path to community files>" % (sys.argv[0])
        print 'Example: $ python %s "data/flickr/community_*.csv"' % (sys.argv[0])
        print "Exiting..."
        exit(-1)    


    p = "community_[0-9]+\.csv$"
    c = re.compile(p)
    files = [_ for _ in files if c.search(_)]
    nr = len(files)
    for i, filename in enumerate(files):
        outfilename = filename + "_data.csv"
        if os.path.isfile(outfilename):
            continue
        print now(), "%d/%d" % (i+1, nr), filename
        calc_data(filename, outfilename)    

