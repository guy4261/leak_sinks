import csv
import re
import sys
import traceback
from collections import Counter

from glob import glob
from gzip import open as _gz_open


exp = Counter()

def cache_flickr_all_photos():
    lines = []
    flickr_all_photos = "data/flickr/flickr-all-photos.txt.gz"
    for line in _gz_open(flickr_all_photos, "r"):
        photoID,timeUp,woner,temp1,temp2,temp3=line.strip().split('\t')
        photoID=int(photoID)
        timeUp=int(timeUp)
        woner=int(woner)
        lines.append( (photoID, timeUp, woner) )
        exp[woner] += 1
        
    return lines


def cache_flickr_all_photo_favorite_markings():
    lines = []
    flickr_all_photo_favorite_markings = "data/flickr/flickr-all-photo-favorite-markings.txt.gz"
    for line in _gz_open(flickr_all_photo_favorite_markings, "r"):
        userId,photoID,timeLike=line.strip().split('\t')  
        userId=int(userId)
        photoID=int(photoID)
        timeLike=int(timeLike)
        lines.append( (userId, photoID, timeLike) )
        exp[userId] += 1
    return lines

flickr_all_photos = cache_flickr_all_photos()
flickr_all_photo_favorite_markings = cache_flickr_all_photo_favorite_markings()


def calc_exposures(comFile, outfilename):
    nodes = set()
    try:
        for line in open(comFile, "r"):
            node1,node2=line.strip().split(",")
            node1 = int(node1)
            node2 = int(node2)    
            nodes.add(node1)
            nodes.add(node2)
    except IOError as err:
        print "I/O error: {0}".format(err)     
    
    outfile1 = file(outfilename, "wb")
    writer1=csv.writer(outfile1,delimiter=',')
    data = [["user","NumberOfExposures"],]
    writer1.writerows(data)
    for user in nodes:
        data = [[user,exp[user]],]        
        writer1.writerows(data)

             
if __name__ == "__main__":
    
    try:
        path = sys.argv[1]
        #path = "data/flickr/flickr_community_multilevel/community_*.csv"
        files = glob(path)
    except:
        traceback.print_exc(file=sys.stdout)
        print "Usage: %s <glob path to community files>" % (sys.argv[0])
        print 'Example: $ python %s "data/flickr/community_*.csv"' % (sys.argv[0])
        print "Exiting..."
        exit(-1)
    

    p = "community_[0-9]+\.csv$"
    c = re.compile(p)
    files = [_ for _ in files if c.search(_)]

    for filename in files:
        outfilename = filename + "_exposures.csv"
        calc_exposures(filename, outfilename)

