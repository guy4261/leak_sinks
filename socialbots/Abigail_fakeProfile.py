import sys
import traceback
import random

import ProbabiltyClass


class Abigail_fakeProfile():

    def __init__(self,graph,direction):
        self.graph = graph
        self.direction=direction
        
    def run(self):
          
        try: 
             allResult = {}
             nodeList = self.graph.nodes()
             nodesCentrality={}
             self.graph.add_node(-1)  #add a node of the bot
            

             for node in nodeList:
                        indegreeProfile = set([x for x,n in self.graph.in_edges(node)])
                        indegreeBot = set([x for x,n in self.graph.in_edges(-1)])
                        outDegreeProfile = set([n for x,n in self.graph.out_edges(node)])
                        outDegreeBot = set([n for x,n in self.graph.out_edges(-1)])
                        prob=ProbabiltyClass.ProbabiltyClass.DeterminingProbability(indegreeBot,indegreeProfile,outDegreeBot,outDegreeProfile,self.direction)
                        i=node,prob
                        nodesCentrality[node]=prob
             size = len(nodeList)
             k = 0
             while(nodeList):
                  
                   if(k == size):
                      break
                   node = max(nodesCentrality, key=nodesCentrality.get)
                 
                   nodeList.remove(node)
                   prob= nodesCentrality[node]
                   del nodesCentrality[node]
                   k = k + 1
                 
                   randomNum = random.random() * 1 
                   if(prob > randomNum):
                      accepted = True
                      self.graph.add_edge(-1,node)
                      indegreeBot = []
                      outDegreeBot = set([n for x,n in self.graph.out_edges(-1)])
                      indegreeProfile = set([x for x,n in self.graph.in_edges(node)])
                      outDegreeProfile = set([n for x,n in self.graph.out_edges(node)])
                      for nodeIter in indegreeProfile:
                          if(nodeIter in nodesCentrality):
                                indegreee = set([x for x,n in self.graph.in_edges(nodeIter)])
                                outDegree = set([n for x,n in self.graph.out_edges(nodeIter)])
                                prob=ProbabiltyClass.ProbabiltyClass.DeterminingProbability(indegreeBot,indegreee,outDegreeBot,outDegree,self.direction)
                                nodesCentrality[nodeIter]=prob
                      for nodeIter in outDegreeProfile:
                          if(nodeIter in nodesCentrality):
                                indegreee = set([x for x,n in self.graph.in_edges(nodeIter)])
                                outDegree = set([n for x,n in self.graph.out_edges(nodeIter)])
                                prob=ProbabiltyClass.ProbabiltyClass.DeterminingProbability(indegreeBot,indegreee,outDegreeBot,outDegree,self.direction)
                                nodesCentrality[nodeIter]=prob
                    
                   else:
                      accepted = False
                          
                   allResult[k] = k,node,accepted

#            PrintToFile.PrintToFile.csvFile(allResult)
#             return self.graph
             return allResult

        except: 
                print traceback.print_exc()
                print sys.exc_info()

