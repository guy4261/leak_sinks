import csv


class PrintToFile():

    @staticmethod
    def csvFile(allResult, filename="friendsTable.csv", header=("k", "nodeID", "accepted?")):
        with open(filename, 'wb') as c:
            writer = csv.writer(c, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
            writer.writerow(header)
            for row in allResult.values():
                writer.writerow(row)
    
    
    @staticmethod
    def yasminFile(allResult, filename):
        
        PrintToFile.csvFile(allResult, filename, header=("k", "who is it", "requests so far", "fail so far"))
         


str2bool = {"True":True, "False":False}

class ReadFromFile():

    @staticmethod
    def csvFile(filename="friendsTable.csv"):
        allResults = {}
        with open(filename, "rb") as c:
            header = c.next()#skip header
      
            reader = csv.reader(c, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

            for row in reader:
                k, nodeID, accepted = row
                k = int(k)
                nodeID = int(nodeID)
                accepted = str2bool[accepted]
                allResults[k] = (k, nodeID, accepted)
        
        return allResults

