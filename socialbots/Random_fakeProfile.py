import random
from random import choice
import sys
import traceback
import ProbabiltyClass

class random_fakeProfile():

    def __init__(self, graph, direction):
        self.graph = graph
        self.direction=direction

    def run(self):
          
        try: 
             allResult = {}
             nodeList = self.graph.nodes()
             self.graph.add_node(-1)  #add a node of the bot
             size = len(nodeList)
             k = 0
             while(nodeList):
                   if(k == size):
                      break
                   k = k + 1
                   node = choice(nodeList) #choose target
                   nodeList.remove(node)
                   indegreeProfile = set([x for x,n in self.graph.in_edges(node)])
                   indegreeBot = set([x for x,n in self.graph.in_edges(-1)])
                   outDegreeProfile = set([n for x,n in self.graph.out_edges(node)])
                   outDegreeBot = set([n for x,n in self.graph.out_edges(-1)])
                      
                   prob = ProbabiltyClass.ProbabiltyClass.DeterminingProbability(indegreeBot,indegreeProfile,outDegreeBot,outDegreeProfile,self.direction)
                   randomNum = random.random() * 1 
                   if(prob > randomNum):
                      accepted = True
                      self.graph.add_edge(-1,node)
                      
                   else:
                      accepted = False
                          
                   allResult[k] = k,node,accepted

             #PrintToFile.PrintToFile.csvFile(allResult)
             #return self.graph
             return allResult

        except:
 
                print traceback.print_exc()
                print sys.exc_info()
