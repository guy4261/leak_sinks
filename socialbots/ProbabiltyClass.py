from operator import itemgetter
import networkx as nx
import random
from random import choice
import sys
from sets import Set
import csv
import networkx
import networkx.algorithms.connectivity
from networkx.algorithms import bipartite
import copy
import math

class ProbabiltyClass(object): 

    @staticmethod
    def DeterminingProbability(indegreeBot,indegreeProfile,outDegreeBot,outDegreeProfile,flag): #gets the 2 list of friends and return the prob for accepting friend 
        tf=0
        cf=0
        if (flag=='in'):
             s1 = list(indegreeProfile.intersection(outDegreeBot))
        else :
             s1 = list(outDegreeProfile.intersection(outDegreeBot))
        count = len(s1)
        if (count==0):
            cf = 0.3
      
        if (count>=11):
            cf = 0.8
        if(count<11 and count>0):
            cf= (0.4+0.035*count)

        length=len(indegreeProfile)
        if (length ==0):
            return 0
        tf= 1-(math.pow(length,-0.064)*1.0889)
        prob= (tf + (1 - tf) * cf)

    
        return prob


