import random
import sys
import traceback
from operator import itemgetter

import networkx as nx

import ProbabiltyClass
import PrintToFile


class PageRank_fakeProfile():

    def __init__(self,graph,direction):
        self.graph = graph
        self.direction=direction

    

    def run(self):
          
        try: 
             allResult = {}
             nodeList = self.graph.nodes()
             self.graph.add_node(-1)  #add a node of the bot
             
             centarlity=nx.pagerank(self.graph)
             nodesCentrality =  sorted(centarlity.iteritems(),key=itemgetter(1),reverse=True)
             size = len(nodeList)
             k = 0
             for node, pr in nodesCentrality:
                   if(k == size):
                      break
                   k = k + 1
                   indegreeProfile = set([x for x,n in self.graph.in_edges(node)])
                   indegreeBot = set([x for x,n in self.graph.in_edges(-1)])
                   outDegreeProfile = set([n for x,n in self.graph.out_edges(node)])
                   outDegreeBot = set([n for x,n in self.graph.out_edges(-1)])
                      
                   prob = ProbabiltyClass.ProbabiltyClass.DeterminingProbability(indegreeBot,indegreeProfile,outDegreeBot,outDegreeProfile,self.direction)
                   randomNum = random.random() * 1 
                   if(prob > randomNum):
                      accepted = True
                      self.graph.add_edge(-1,node)
                    
                   else:
                      accepted = False
                          
                   allResult[k] = k,node,accepted

             return allResult

        except:
 
                print traceback.print_exc()
                print sys.exc_info()

