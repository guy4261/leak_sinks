from operator import itemgetter
import networkx as nx
import random
from random import choice
import sys
import os
import math
import logging
import traceback
import timeit
from sets import Set
import csv
import ProbabiltyClass
import PrintToFile



class MickyFire_fakeProfile():

    def __init__(self,graph,direction):
        self.graph = graph
        self.direction=direction

    

    def run(self):
          
        try: 
             allResult = {}
             nodeList = self.graph.nodes()
              #add a node of the bot
             count=0
             deg=self.graph.in_degree_iter()#return an iterator directly, saving memory
             powers = sorted(deg,key=itemgetter(1),reverse=True) #sort all the graph by num of friend
             self.graph.add_node(-1) 
             size = len(nodeList)
             k = 0


             for node,deg in powers:
                   if( (k == size) or(count==50)):
                     break
                   k = k + 1
                   nodeList.remove(node)
                   indegreeProfile = set([x for x,n in self.graph.in_edges(node)])
                   indegreeBot = set([x for x,n in self.graph.in_edges(-1)])
                   outDegreeProfile = set([n for x,n in self.graph.out_edges(node)])
                   outDegreeBot = set([n for x,n in self.graph.out_edges(-1)])
                      
                   prob = ProbabiltyClass.ProbabiltyClass.DeterminingProbability(indegreeBot,indegreeProfile,outDegreeBot,outDegreeProfile,self.direction)
                   randomNum = random.random() * 1 
                   if(prob > randomNum):
                      accepted = True
                      count = count+1
                      self.graph.add_edge(-1,node)
                     
                   else:
                      accepted = False
                      
                   allResult[k] = k,node,accepted


             mutualfriends={}
             outDegreeBot = set([n for x,n in self.graph.out_edges(-1)])
             indegreeBot = set([x for x,n in self.graph.in_edges(-1)])
             for nodeop in nodeList:
               
              indegreeProfile = set([x for x,n in self.graph.in_edges(nodeop)])
              outDegreeProfile = set([n for x,n in self.graph.out_edges(nodeop)])
              if(self.direction=="out"):
                  s1 = len(list(outDegreeProfile.intersection(outDegreeBot)))
              if(self.direction=="in"):
                  s1 = len(list(indegreeProfile.intersection(outDegreeBot)))
              
              mutualfriends[nodeop]=s1


             
             while(nodeList):
               if(k == size):
                  break
               k = k + 1
               node  = max(mutualfriends, key=mutualfriends.get)
            
               nodeList.remove(node)
               del mutualfriends[node]
               indegreeProfile = set([x for x,n in self.graph.in_edges(node)])
               indegreeBot = set([x for x,n in self.graph.in_edges(-1)])
               outDegreeProfile = set([n for x,n in self.graph.out_edges(node)])
               outDegreeBot = set([n for x,n in self.graph.out_edges(-1)])
                      
               prob = ProbabiltyClass.ProbabiltyClass.DeterminingProbability(indegreeBot,indegreeProfile,outDegreeBot,outDegreeProfile,self.direction)
               randomNum = random.random() * 1 
               if(prob > randomNum):
                      accepted = True
                      count = count+1
                      self.graph.add_edge(-1,node)
                      indegreeBot = []
                      outDegreeBot = set([n for x,n in self.graph.out_edges(-1)])
                      for node1 in indegreeProfile:
                          if(node1 in  mutualfriends):
                              ine = set([x for x,n in self.graph.in_edges(node1)])
                              out = set([n for x,n in self.graph.out_edges(node1)])
                              if(self.direction=="out"):
                                  s1 = len(list(out.intersection(outDegreeBot)))
                              if(self.direction=="in"):
                                  s1 = len(list(ine.intersection(outDegreeBot)))
                              mutualfriends[node1]=s1
                      for node2 in outDegreeProfile:
                          if(node2 in  mutualfriends):
                              ine = set([x for x,n in self.graph.in_edges(node2)])
                              out = set([n for x,n in self.graph.out_edges(node2)])
                              if(self.direction=="out"):
                                  s1 = len(list(out.intersection(outDegreeBot)))
                              if(self.direction=="in"):
                                  s1 = len(list(ine.intersection(outDegreeBot)))
                              mutualfriends[node2]=s1

                              
                    
               else:
                      accepted = False
                     
               allResult[k] = k,node,accepted
              


             
             #PrintToFile.PrintToFile.csvFile(allResult)
             return allResult
             #return self.graph

        except:
 
                print traceback.print_exc()
                print sys.exc_info()

 
                     



