from operator import itemgetter
import networkx as nx
import random
from random import choice
import sys
import os
import math
import logging
import traceback
import timeit
from sets import Set
import csv
import ProbabiltyClass
import PrintToFile



class indegree_fakeprofile():

    def __init__(self,graph,direction):
        self.graph = graph
        self.direction=direction

    

    def run(self):
          
        try: 
             allResult = {}
             nodeList = self.graph.nodes()


             centarlity={}
             for n in nodeList:
                    centarlity[n]= len(self.graph.in_edges(n))
            
             nodesCentrality =  sorted(centarlity.iteritems(),key=itemgetter(1),reverse=True)
             self.graph.add_node(-1)  #add a node of the bot
             
           
             size = len(nodeList)
             k = 0
             for node,pr in nodesCentrality:
                   if(k == size):
                      break
                   k = k + 1
                   indegreeProfile = set([x for x,n in self.graph.in_edges(node)])
                   indegreeBot = set([x for x,n in self.graph.in_edges(-1)])
                   outDegreeProfile = set([n for x,n in self.graph.out_edges(node)])
                   outDegreeBot = set([n for x,n in self.graph.out_edges(-1)])
                      
                   prob = ProbabiltyClass.ProbabiltyClass.DeterminingProbability(indegreeBot,indegreeProfile,outDegreeBot,outDegreeProfile,self.direction)
                   randomNum = random.random() * 1 
                   if(prob > randomNum):
                      accepted = True
                      self.graph.add_edge(-1,node)
                    
                   else:
                      accepted = False
                          
                   allResult[k] = k,node,accepted

             #PrintToFile.PrintToFile.csvFile(allResult)
             #return self.graph
             return allResult             

        except:
 
                print traceback.print_exc()
                print sys.exc_info()

 
                     



